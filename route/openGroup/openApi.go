package openGroup

import (
	"github.com/labstack/echo"
	"tigoApi/controller"
)

func UnauthenticatedGroup(g *echo.Group) {
	g.GET("testHashing", controller.SetHash)
	//g.GET("setSession", controller.SetUserSession, middleware.DefaultProperties())
}