package route

import (
	"github.com/labstack/echo"
	middlewareEcho "github.com/labstack/echo/middleware"
	middleware "tigoApi/adapters"
	"tigoApi/controller"
	"tigoApi/route/openGroup"
	"tigoApi/route/tokenGroup"
)

func Init() *echo.Echo {
	e := echo.New()
	e.Use(middlewareEcho.Logger())
	h, _ := controller.NewHandler()
	//e.Static(fmt.Sprintf("/%s", "gallery"), "assets")
	initGroup := e.Group("api/")
	initGroup.Use(middleware.DefaultProperties())
	graphqlGroup := e.Group("graphqlRequest/")
	e.POST("graphql", echo.WrapHandler(h))
	securedGroup := e.Group("secured/")
	openGroup.UnauthenticatedGroup(graphqlGroup)
	middleware.SetCookieMiddleware(securedGroup)
	openGroup.UnauthenticatedGroup(initGroup)
	tokenGroup.SecuredGroup(securedGroup)
	return e
}
