package config

import (
	"bufio"
	"errors"
	"io"
	"os"
	"regexp"
	"strings"
	"sync"
)

// Environment retorna las variables de entorno como constantes
type Environment struct {
	// Entorno de desarrollo: Prod, Desarrollo
	Environment string `name:"env"`

	// SQL_Server variables
	SqlServerUrl string `name:"sqlserver_url"`
	SqlServerPort string `name:"sqlserver_port"`
	SqlServerUser string `name:"sqlserver_user"`
	SqlServerPassword string `name:"sqlserver_password"`
	PrivateKey string `name:"private_key"`
	JwtSignature string `name:"jwt_signature"`
	// Variables para SAL Y PIMIENTA
	SaltedPassword string `name:"salted_pass"`
	PepperPassword string `name:"pepper_pass"`
	// Variables de lenguaje
	Language string `name:"language"`
	// GraphiQLEnable variables
	GraphiQLEnable bool `name:"graphiql_enable"`
}

//Sincro sera una funcion que observara si hay cambios en el archivo, y actuara como unica variable de acceso
var sincro sync.Once
//Enviromnent es una variable de nuestro struct, el signo * se usa para decir que sera un objeto constante o persistente.
var environment *Environment

//Instance sera la instancia global de las variables de sesion
func Instance() *Environment{
	sincro.Do(func() {
		//Si el archivo se carga con exito retorna un objeto en caso contrario retorna un objeto nulo
		if loadFile(".env") != nil {
			environment = nil
		}else{
			environment = &Environment{
				Environment: os.Getenv("ENV"),
				SqlServerUrl: os.Getenv("SQL_SERVER_URL"),
				SqlServerPort: os.Getenv("SQL_SERVER_PORT"),
				SqlServerUser: os.Getenv("SQL_SERVER_USER"),
				SqlServerPassword: os.Getenv("SQL_SERVER_PASSWORD"),
				PrivateKey: os.Getenv("PRIVATE_KEY"),
				SaltedPassword: os.Getenv("SALTED_PASS"),
				PepperPassword: os.Getenv("PEPPER_PASS"),
				JwtSignature:os.Getenv("JWT_SIGNATURE"),
				GraphiQLEnable: os.Getenv("GRAPHIQL_ENABLE") == "true",
				Language: os.Getenv("API_LANGUAGE"),
			}
		}
	})
	return environment
}

func loadFile(filename string) error {
	envMap, err := readFile(filename)
	if err != nil {
		return err
	}

	currentEnv := map[string]bool{}
	rawEnv := os.Environ()
	for _, rawEnvLine := range rawEnv {
		key := strings.Split(rawEnvLine, "=")[0]
		currentEnv[key] = true
	}

	for key, value := range envMap {
		if !currentEnv[key] {
			if os.Getenv(key) == "" {
				os.Setenv(key, value)
			}
		}
	}
	return nil
}

func readFile(filename string) (envMap map[string]string, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()

	return parse(file)
}

func parse(r io.Reader) (envMap map[string]string, err error) {
	envMap = make(map[string]string)

	var lines []string
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err = scanner.Err(); err != nil {
		return
	}

	for _, fullLine := range lines {
		if !isIgnoredLine(fullLine) {
			var key, value string
			key, value, err = parseLine(fullLine, envMap)

			if err != nil {
				return
			}
			envMap[key] = value
		}
	}
	return
}

func parseLine(line string, envMap map[string]string) (key string, value string, err error) {
	if len(line) == 0 {
		err = errors.New("zero length string")
		return
	}

	// Deshazte de los comentarios (pero mantén los hash cifrados)
	if strings.Contains(line, "#") {
		segmentsBetweenHashes := strings.Split(line, "#")
		quotesAreOpen := false
		var segmentsToKeep []string
		for _, segment := range segmentsBetweenHashes {
			if strings.Count(segment, "\"") == 1 || strings.Count(segment, "'") == 1 {
				if quotesAreOpen {
					quotesAreOpen = false
					segmentsToKeep = append(segmentsToKeep, segment)
				} else {
					quotesAreOpen = true
				}
			}

			if len(segmentsToKeep) == 0 || quotesAreOpen {
				segmentsToKeep = append(segmentsToKeep, segment)
			}
		}

		line = strings.Join(segmentsToKeep, "#")
	}

	firstEquals := strings.Index(line, "=")
	firstColon := strings.Index(line, ":")
	splitString := strings.SplitN(line, "=", 2)
	if firstColon != -1 && (firstColon < firstEquals || firstEquals == -1) {
		//esta es una línea de yaml-style
		splitString = strings.SplitN(line, ":", 2)
	}

	if len(splitString) != 2 {
		err = errors.New("can't separate key from value")
		return
	}

	// Parsea la llave
	key = splitString[0]
	if strings.HasPrefix(key, "export") {
		key = strings.TrimPrefix(key, "export")
	}
	key = strings.Trim(key, " ")
	// Parsea el valor
	value = parseValue(splitString[1], envMap)
	return
}

func isIgnoredLine(line string) bool {
	trimmedLine := strings.Trim(line, " \n\t")
	return len(trimmedLine) == 0 || strings.HasPrefix(trimmedLine, "#")
}

func parseValue(value string, envMap map[string]string) string {
	//trim
	value = strings.Trim(value, " ")
	//comprueba si tenemos valores cotizados o posibles escapes
	if len(value) > 1 {
		rs := regexp.MustCompile(`\A'(.*)'\z`)
		singleQuotes := rs.FindStringSubmatch(value)

		rd := regexp.MustCompile(`\A"(.*)"\z`)
		doubleQuotes := rd.FindStringSubmatch(value)

		if singleQuotes != nil || doubleQuotes != nil {
			//saca las citas de los bordes
			value = value[1 : len(value)-1]
		}

		if doubleQuotes != nil {
			// expand nueva linea
			escapeRegex := regexp.MustCompile(`\\.`)
			value = escapeRegex.ReplaceAllStringFunc(value, func(match string) string {
				c := strings.TrimPrefix(match, `\`)
				switch c {
				case "n":
					return "\n"
				case "r":
					return "\r"
				default:
					return match
				}
			})
			//caracteres no filtrados
			e := regexp.MustCompile(`\\([^$])`)
			value = e.ReplaceAllString(value, "$1")
		}

		if singleQuotes == nil {
			value = expandVariables(value, envMap)
		}
	}

	return value
}

func expandVariables(v string, m map[string]string) string {
	r := regexp.MustCompile(`(\\)?(\$)(\()?{?([A-Z0-9_]+)?}?`)
	return r.ReplaceAllStringFunc(v, func(s string) string {
		submatch := r.FindStringSubmatch(s)
		if submatch == nil {
			return s
		}
		if submatch[1] == "\\" || submatch[2] == "(" {
			return submatch[0][1:]
		} else if submatch[4] != "" {
			return m[submatch[4]]
		}
		return s
	})
}
