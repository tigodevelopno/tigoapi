package models

type File struct {
	Path string `json:"path"`
	Size int64  `json:"size"`
}
