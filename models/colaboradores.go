package models

type Colaboradores struct {
	Id int `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Status bool `json:"status,omitempty"`
}