package models

import "time"

type User struct {
	Id int `json:"id,omitempty"`
	Colaborador Colaboradores `json:"colaborador,omitempty"`
	Name string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
	Image string `json:"image,omitempty"`
	Password string `json:"password,omitempty"`
	CreatedAt time.Time `json:"createdAt,omitempty"`
}