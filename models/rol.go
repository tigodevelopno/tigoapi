package models

type Rol struct {
	Id int `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Status bool `json:"status,omitempty"`
}