package objects

import (
	"fmt"
	"github.com/graphql-go/graphql"
	"tigoApi/database/driver"
	dbImplement "tigoApi/database/repository/implementation"
	"tigoApi/models"
)

var User = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "User",
		Fields: graphql.Fields{
			"id":        &graphql.Field{Type: graphql.ID},
			"name":      &graphql.Field{Type: graphql.String},
			"email":	 &graphql.Field{Type: graphql.String},
			"password":  &graphql.Field{Type: graphql.String},
			"createdAt": &graphql.Field{Type: graphql.String},
			"colaborador": &graphql.Field{Type: Colaborador,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					user, _ := p.Source.(models.User)
					colaboradorRepository := dbImplement.NewColaboradoresRepo(driver.Instance().SQL)
					data, error := colaboradorRepository.GetByUserId(user.Id)
					if error != nil {
						fmt.Println("error" + error.Error())
						return nil, error
					}
					return data, nil
			},},
		},
		Description: "Users Information",
	},
)