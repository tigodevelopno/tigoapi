package objects

import "github.com/graphql-go/graphql"

var Colaborador = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Colaborador",
		Fields: graphql.Fields{
			"id":        &graphql.Field{Type: graphql.ID},
			"name":      &graphql.Field{Type: graphql.String},
			"telephone": &graphql.Field{Type: graphql.String},
			"status":    &graphql.Field{Type: graphql.Boolean},
		},
		Description: "Colaborador Information",
	},
)

/*
type Colaboradores struct {
	Id int `json:"id,omitempty"`
	Rol Rol `json:"rol,omitempty"`
	Name string `json:"name,omitempty"`
	Telephone string `json:"telephone,omitempty"`
	Status bool `json:"status,omitempty"`
}
 */