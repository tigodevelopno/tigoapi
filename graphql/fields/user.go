package fields

import (
	"database/sql"
	"github.com/graphql-go/graphql"
	dbImplement "tigoApi/database/repository/implementation"
	"tigoApi/graphql/objects"
)

func UserField(SQL *sql.DB) *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(objects.User),
		Resolve: func(p graphql.ResolveParams) (i interface{}, e error) {
			userRepository := dbImplement.NewUserRepo(SQL)
			data, error := userRepository.GetAll()
			if error != nil{
				return nil, error
			}
			return data, nil
		},
		Description: "user",
	}
}