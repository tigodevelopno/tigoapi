package queries

import (
	"database/sql"
	"github.com/graphql-go/graphql"
	"tigoApi/graphql/fields"
)

func NewQuery(SQL *sql.DB) *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"Users": fields.UserField(SQL),
		},
	})
}
