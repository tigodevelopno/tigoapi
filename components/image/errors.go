package image

import "errors"

var (
	ErrDisallowedType = errors.New("file type is not allowed")
	ErrFileSize = errors.New("file size exceeds max limit")
)
