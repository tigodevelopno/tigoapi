package components

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"log"
	"net/http"
	"strings"
	"tigoApi/components/response"
	"tigoApi/config"
	"tigoApi/models"
	"time"
)

func WriteCookie(token string) *http.Cookie {
	cookie := new(http.Cookie)
	cookie.Name = "tigo_token"
	cookie.Value = token
	cookie.Secure = false
	cookie.HttpOnly = true
	cookie.Path = "/"
	cookie.Expires = time.Now().Add(30 * time.Minute)
	return cookie
}

func CheckCookie(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		cookie, err := c.Cookie("tigo_token")
		if err != nil {
			if strings.Contains(err.Error(), "named cookie not present") {
				return response.Error(c,http.StatusForbidden, "Application.Bundle.Error")
			}
			log.Println(err)
			return err
		}
		if cookie.Name == "tigo_token" {
			token := cookie.Value
			claims := new(models.JwtClaims)
				tkn, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
					return config.Instance().JwtSignature, nil
				})
				if err != nil {
					if err == jwt.ErrSignatureInvalid {
						return response.Error(c, http.StatusForbidden, "Application.Bundle.Error")
					}
					return response.Error(c, http.StatusForbidden, "Application.Bundle.Error")
				}
				if !tkn.Valid {
					return response.Error(c, http.StatusForbidden, "Application.Bundle.Error")
				}
				jwtToken, err := SetJWToken(c.Request().Header.Get("platformImei"))
				if err != nil {
					return response.Error(c, http.StatusInternalServerError, "Something went wrong on the DarkSide of the Force")
				}
				cookie := WriteCookie(jwtToken)
				c.SetCookie(cookie)
			return next(c)
		}
		return response.Error(c, http.StatusForbidden, "Application.Bundle.Error")
	}
}