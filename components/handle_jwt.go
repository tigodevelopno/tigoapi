package components

import (
	"github.com/dgrijalva/jwt-go"
	"tigoApi/config"
	"tigoApi/models"
	"time"
)

var Time time.Time

func SetJWToken(imei string) (string, error) {
	Time = time.Now().Add(30 * time.Minute)
	claims := models.JwtClaims{
		Username: imei,
		UserID:   imei,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: Time.Unix(),
			Issuer:    "EOF",
			Audience:  "EXCEPTION",
		},
	}
	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := rawToken.SignedString(config.Instance().JwtSignature)
	if err != nil {
		return "", err
	}
	return token, nil
}
