package adapters

import (
	"github.com/labstack/echo"
	"net/http"
	"strings"
	"tigoApi/components/response"
)

func DefaultProperties() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			owner := c.Request().Header.Get("ownerApi")
			if !strings.HasPrefix(owner, "TIGO") {
				return response.Error(c, http.StatusUnauthorized, "application.error.bundle")
			}
			if owner == "" {
				return response.Error(c, http.StatusBadRequest, "Application.Error.Bundle")
			}
			return next(c)
		}
	}
}
