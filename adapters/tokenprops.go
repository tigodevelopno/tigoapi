package adapters

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"tigoApi/components"
	"tigoApi/config"
)

func SetJwtMiddlewares(g *echo.Group) {
	g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningMethod: "HS256",
		SigningKey:    config.Instance().JwtSignature,
		TokenLookup: "cookie:tigo_token",
	}))
}

func SetCookieMiddleware(g *echo.Group){
	g.Use(components.CheckCookie)
}