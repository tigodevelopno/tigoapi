package main

import (
	"tigoApi/route"
	"tigoApi/utils"
)

func main(){
	e := route.Init()
	port := utils.GetPort()
	e.Logger.Fatal(e.Start(port))
	//e.Logger.Fatal(e.StartTLS(port, "./certs/cert.pem","./certs/key.pem"))
}
//env, _ := config.New()
/*
fmt.Println(config.Instance().Environment)
 */