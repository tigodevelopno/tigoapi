package driver

import (
	"sync"
	"tigoApi/config"
)

var connection *SQLServerDB
var sincro sync.Once

func Instance() *SQLServerDB {
	sincro.Do(func() {
		connection = NewDbManage()
	})
	return connection
}

func NewDbManage() *SQLServerDB {
	if config.Instance().Environment == "local" {
		db := Connect("localhost","1433","sa","Lacb2208.","Master_POC")
		return db
	}else{
		db := Connect("host","port","user","password","dbname")
		return db
	}
}
