package driver

import (
	"database/sql"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
)

type SQLServerDB struct {
	SQL *sql.DB
}

var SQLServer = &SQLServerDB{}

func Connect(server, port, user, password, dbname string) *SQLServerDB {
	connStr := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%s;database=%s", server, user, password, port, dbname)
	db, err := sql.Open("mssql", connStr)
	if err != nil {
		panic(err)
	}
	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(10)
	SQLServer.SQL = db
	return SQLServer
}

func (p *SQLServerDB) Close() error {
	return p.Close()
}