package _interface

import "tigoApi/models"

type ColaboradoresRepo interface {
	GetAll() ([]models.Colaboradores, error)
	GetById(id int) (models.Colaboradores, error)
	GetByUserId(userId int) (*models.Colaboradores, error)
	SelectByUser(filter models.User) ([]models.Colaboradores, error)
	Insert(b models.Colaboradores) error
	Update(b models.Colaboradores) error
}
