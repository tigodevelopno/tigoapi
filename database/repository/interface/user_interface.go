package _interface

import "tigoApi/models"

type UserRepo interface {
	Login(user models.User) (bool, error)
	Logout()
	GetAll() ([]models.User, error)
	//Select() ([]models.UserSession, error)
	//SearchSession() (models.UserSession, error)
}

