package implementation

import (
	"database/sql"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
	repo "tigoApi/database/repository/interface"
	"tigoApi/models"
)

type UserRepoImpl struct {
	Db *sql.DB
}

func NewUserRepo(db *sql.DB) repo.UserRepo {
	return &UserRepoImpl {
		Db: db,
	}
}

func (u *UserRepoImpl) Login(user models.User) (bool, error) {
	panic("implement me")
}
/*
	dbImplement "tigoApi/database/repository/implementation"
	businessRepo := dbImplement.NewUserRepo(driver.Instance().SQL)
	data, error := businessRepo.GetAll()
 */
func (u *UserRepoImpl) GetAll() ([]models.User, error) {
	user_list := make([]models.User, 0)
	rows, err := u.Db.Query("SELECT * FROM USUARIOS")
	if err != nil {
		return user_list, err
	}
	for rows.Next() {
		user := models.User{}
		var id_colaborador int
		err := rows.Scan(&user.Id, &id_colaborador, &user.Name, &user.Email, &user.Image, &user.Password, &user.CreatedAt)
		if err !=  nil {
			fmt.Println(err)
			break
		}
		user.Colaborador, err = NewColaboradoresRepo(u.Db).GetById(id_colaborador)
		if err != nil {
			break
		}
		user_list = append(user_list, user)
	}
	err = rows.Err()
	if err != nil {
		return user_list, err
	}
	fmt.Println(user_list)
	return user_list, nil
}

func (u *UserRepoImpl) Logout() {
	panic("implement me")
}

/*func (b *BusinessRepoImpl) GetDetailedBusiness(businessId int) (*models.Business, error) {
	var location = ""
	//businessId = sanitizer.Escape(businessId)
	//businessId = sanitizer.StructuredSanitation("%s", businessId)
	business := new(models.Business)
	query, err := b.Db.Prepare("SELECT business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category, array(select departments.department from listDepartments() as departments where departments.business = business.id) as departments FROM Business INNER JOIN category on category.id = business.id_category WHERE business.id = $1 group by business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category")
	if err != nil {
		return business, err
	}
	rows, err := query.Query(businessId)
	if err != nil {
		return business, err
	}
	for rows.Next() {
		err := rows.Scan(&business.ID, &business.Name, &business.Owner, &location, &business.Reason, &business.Category, pq.Array(&business.Departments))
		business.Images = utils.DirwalkStrings("./assets")
		if err := json.Unmarshal([]byte(location), &business.Location); err != nil { panic(err) }
		if err !=  nil {
			break
		}
	}
	err = rows.Err()
	if err != nil {
		return business, err
	}
	return business, nil
}

func (b *BusinessRepoImpl) Select() ([]models.Business, error) {
	var location = ""
	business_list := make([]models.Business, 0)
	rows, err := b.Db.Query("SELECT business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category, array(select departments.department from listDepartments() as departments where departments.business = business.id) as departments FROM business INNER JOIN category on category.id = business.id_category  group by business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category")
	if err != nil {
		return business_list, err
	}
	for rows.Next() {
		business := models.Business{}
		err := rows.Scan(&business.ID, &business.Name, &business.Owner, &location, &business.Reason, &business.Category, pq.Array(&business.Departments))
		business.Images = utils.DirwalkStrings("./assets")
		if err := json.Unmarshal([]byte(location), &business.Location); err != nil { panic(err) }
		if err !=  nil {
			fmt.Println(err)
			break
		}
		business_list = append(business_list, business)
	}
	err = rows.Err()
	if err != nil {
		return business_list, err
	}
	return business_list, nil
}

func (b *BusinessRepoImpl) SelectWithDepartments() ([]models.Business, error) {
	var location = ""
	business_list := make([]models.Business, 0)
	rows, err := b.Db.Query("SELECT business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category, array(select departments.department from listDepartments() as departments where departments.business = business.id) as departments FROM business INNER JOIN category on category.id = business.id_category  group by business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category")
	if err != nil {
		fmt.Println(err)
		return business_list, err
	}
	for rows.Next() {
		business := models.Business{}
		err := rows.Scan(&business.ID, &business.Name, &business.Owner, &location, &business.Reason, &business.Category, pq.Array(&business.Departments))
		business.Images = utils.DirwalkStrings("./assets")
		if err := json.Unmarshal([]byte(location), &business.Location); err != nil { panic(err) }
		if err !=  nil {
			fmt.Println(err)
			break
		}
		business_list = append(business_list, business)
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		return business_list, err
	}
	return business_list, nil
}

func (b *BusinessRepoImpl) SelectByBusiness(filter models.Filters) ([]models.Business, error) {
	var location = ""
	filter.Business = sanitizer.Escape(filter.Business)
	filter.Business = sanitizer.StructuredSanitation("%s", filter.Business)
	business_list := make([]models.Business, 0)
	query, err := b.Db.Prepare("SELECT business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category, array(select departments.department from listDepartments() as departments where departments.business = business.id) as departments FROM Business INNER JOIN category on category.id = business.id_category WHERE business.name ilike $1 or business.owner ilike $1 group by business.id, business.name, business.owner, business.coordinates, business.reason_froggy, category.category")
	if err != nil {
		return business_list, err
	}
	rows, err := query.Query(filter.Business)
	if err != nil {
		return business_list, err
	}
	for rows.Next() {
		business := models.Business{}
		err := rows.Scan(&business.ID, &business.Name, &business.Owner, &location, &business.Reason, &business.Category, pq.Array(&business.Departments))
		business.Images = utils.DirwalkStrings("./assets")
		if err := json.Unmarshal([]byte(location), &business.Location); err != nil { panic(err) }
		if err !=  nil {
			break
		}
		business_list = append(business_list, business)
	}
	err = rows.Err()
	if err != nil {
		return business_list, err
	}
	return business_list, nil
}

func (b *BusinessRepoImpl) Insert(business models.Business) error {
	//insertStatement := `INSERT INTO business(id, name, gender, email) VALUES ($1, $2, $3, $4)`

	//_, err := b.Db.Exec(insertStatement, user.ID, user.Name, user.Gender, user.Email)
	//if err != nil {
	//	return err
	//}

	fmt.Println("Record added: ", business)
	return nil
}

func (b *BusinessRepoImpl) Update(business models.Business) error {
	panic("implement me")
}*/