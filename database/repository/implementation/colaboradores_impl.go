package implementation

import (
	"database/sql"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
	repo "tigoApi/database/repository/interface"
	"tigoApi/models"
)

type ColaboradoresRepoImpl struct {
	Db *sql.DB
}

func NewColaboradoresRepo(db *sql.DB) repo.ColaboradoresRepo {
	return &ColaboradoresRepoImpl {
		Db: db,
	}
}

func (c ColaboradoresRepoImpl) GetByUserId(userId int) (*models.Colaboradores, error) {
	colaborador := new(models.Colaboradores)
	query, err := c.Db.Prepare("SELECT COLABORADORES.id, COLABORADORES.nombre, COLABORADORES.num_telefono, COLABORADORES.estado FROM COLABORADORES INNER JOIN USUARIOS ON USUARIOS.ID_COLABORADOR = COLABORADORES.ID WHERE USUARIOS.ID = ?")
	if err != nil {
		fmt.Println(err)
		return colaborador, err
	}
	rows, err := query.Query(userId)
	if err != nil {
		fmt.Println(err)
		return colaborador, err
	}
	for rows.Next() {
		err := rows.Scan(&colaborador.Id, &colaborador.Name, &colaborador.Telephone, &colaborador.Status)
		if err !=  nil {
			fmt.Println(err)
			break
		}
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		return colaborador, err
	}
	fmt.Println("lo halle")
	fmt.Println(colaborador)
	return colaborador, nil
}

func (c ColaboradoresRepoImpl) GetAll() ([]models.Colaboradores, error) {
	colaboradores_list := make([]models.Colaboradores, 0)
	rows, err := c.Db.Query("SELECT * FROM COLABORADORES")
	if err != nil {
		return colaboradores_list, err
	}
	for rows.Next() {
		colaborador := models.Colaboradores{}
		err := rows.Scan(&colaborador.Id)
		if err !=  nil {
			break
		}
		colaboradores_list = append(colaboradores_list, colaborador)
	}
	err = rows.Err()
	if err != nil {
		return colaboradores_list, err
	}
	return colaboradores_list, nil
}

func (c ColaboradoresRepoImpl) GetById(id int) (models.Colaboradores, error) {
	colaborador := new(models.Colaboradores)
	query, err := c.Db.Prepare("SELECT id, nombre, num_telefono, estado FROM COLABORADORES WHERE COLABORADORES.ID = ?")
	if err != nil {
		fmt.Println(err)
		return *colaborador, err
	}
	rows, err := query.Query(id)
	if err != nil {
		fmt.Println(err)
		return *colaborador, err
	}
	for rows.Next() {
		err := rows.Scan(&colaborador.Id, &colaborador.Name, &colaborador.Telephone, &colaborador.Status)
		if err !=  nil {
			fmt.Println(err)
			break
		}
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		return *colaborador, err
	}
	return *colaborador, nil
}

func (c ColaboradoresRepoImpl) SelectByUser(filter models.User) ([]models.Colaboradores, error) {
	colaboradores_list := make([]models.Colaboradores, 0)
	rows, err := c.Db.Query("SELECT * FROM COLABORADORES WHERE COLABORADORES.ID = ?")
	if err != nil {
		return colaboradores_list, err
	}
	for rows.Next() {
		colaborador := models.Colaboradores{}
		err := rows.Scan(&colaborador.Id)
		if err !=  nil {
			break
		}
		colaboradores_list = append(colaboradores_list, colaborador)
	}
	err = rows.Err()
	if err != nil {
		return colaboradores_list, err
	}
	fmt.Println(colaboradores_list)
	return colaboradores_list, nil
}

func (c ColaboradoresRepoImpl) Insert(b models.Colaboradores) error {
	panic("implement me")
}

func (c ColaboradoresRepoImpl) Update(b models.Colaboradores) error {
	panic("implement me")
}