package controller

import (
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"tigoApi/database/driver"
	"tigoApi/graphql/queries"
)

func NewHandler() (*handler.Handler, error) {
	schema, err := graphql.NewSchema(
		graphql.SchemaConfig{
			Query: queries.NewQuery(driver.Instance().SQL),
		},
	)
	if err != nil {
		return nil, err
	}

	return handler.New(&handler.Config{
		Schema:   &schema,
		Pretty:   true,
		GraphiQL: true,
	}), nil
}
