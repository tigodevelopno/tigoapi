package controller

import (
	"fmt"
	"github.com/labstack/echo"
	"tigoApi/components/response"
	"tigoApi/database/driver"
	dbImplement "tigoApi/database/repository/implementation"
)

func SetHash(c echo.Context) error {
	/*
		var wg sync.WaitGroup
		hashedhPassword := make(chan string)
		wg.Add(1)
	 */
	userRepository := dbImplement.NewUserRepo(driver.Instance().SQL)
	data, error := userRepository.GetAll()
	if error != nil{
		fmt.Print(error)
	}
	//fmt.Println(data)
	//hashedhPassword := make(chan string)
	//go utils.GenerateSaltedHashAsync("lacb2208", hashedhPassword)
	return response.Success(c, data)
}