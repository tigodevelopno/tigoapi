package utils

import (
	"bytes"
	"image"
	"image/jpeg"
	"image/png"
	"strings"
)

func DataToImage(data []byte, imageName string) (image.Image, error) {
	reader := bytes.NewReader(data)
	var imageExtension = strings.Split(imageName, ".")[1]
	var img image.Image
	var err error
	switch imageExtension {
	case "png":
		img, err = png.Decode(reader)
	case "jpg":
		img, err = jpeg.Decode(reader)
	case "jpeg":
		img, err = jpeg.Decode(reader)
	default:
		img = nil
	}
	if err != nil {
		return img, err
	}
	return img, err
}
