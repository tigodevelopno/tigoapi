package utils

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"errors"
	"fmt"
	"golang.org/x/crypto/argon2"
	"strconv"
	"strings"
	"tigoApi/config"
)

const (
	Time         = 4
	Memory       = 64 * 1024
	KeyLen       = 80
	Threads      = 10
)

var environment = config.Instance()

func generateSalt(len int) (string, error) {
	unencodedSalt := make([]byte, len)
	if _, err := rand.Read(unencodedSalt); err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(unencodedSalt), nil
}

func GenerateSaltedHashAsync(password string, hashedPassword chan string) {
	cryptoKey := argon2.IDKey([]byte(password), []byte(environment.SaltedPassword), Time, Memory, uint8(Threads), KeyLen)
	encodedPassword := base64.StdEncoding.EncodeToString(cryptoKey)
	consoleHash := fmt.Sprintf("%s$%d$%d$%d$%d$%s$%s", environment.PepperPassword, Time, Memory, Threads, KeyLen, environment.SaltedPassword, encodedPassword)
	hashedPassword <- consoleHash
	close(hashedPassword)
}

/*
func GenerateSaltedHashAsync(password string,wg *sync.WaitGroup, hashedPassword chan string) {
	cryptoKey := argon2.IDKey([]byte(password), []byte(environment.SaltedPassword), Time, Memory, uint8(Threads), KeyLen)
	encodedPassword := base64.StdEncoding.EncodeToString(cryptoKey)
	consoleHash := fmt.Sprintf("%s$%d$%d$%d$%d$%s$%s", environment.PepperPassword, Time, Memory, Threads, KeyLen, environment.SaltedPassword, encodedPassword)
	defer wg.Done()
	hashedPassword <- consoleHash
	wg.Wait()
	close(hashedPassword)
}
 */

func CompareHashWithPassword(hash, password string) (bool, error) {
	hashParts := strings.Split(hash, "$")
	if len(hashParts) != 7 {
		return false, errors.New("invalid password hash")
	}
	time, _ := strconv.Atoi(hashParts[1])
	memory, _ := strconv.Atoi(hashParts[2])
	threads, _ := strconv.Atoi(hashParts[3])
	keyLen, _ := strconv.Atoi(hashParts[4])
	key, _ := base64.StdEncoding.DecodeString(hashParts[6])
	calculatedKey := argon2.IDKey([]byte(password), []byte(environment.SaltedPassword), uint32(time), uint32(memory), uint8(threads), uint32(keyLen))
	if subtle.ConstantTimeCompare(key, calculatedKey) != 1 {
		return false, errors.New("password did not match")
	}
	return true, nil
}