package utils

import (
	"GoMegaBoicot/models"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"strings"
)

func GetPort() string {
	p := os.Getenv("PORT")
	if p != "" {
		return ":" + p
	}
	return ":3000"
}

var realPath string

func GetRealPath() string{
	path, err := os.Getwd()
	if err != nil {
		return ""
	}
	return path
}

func Dirwalk(dir string) (files []models.File, err error) {
	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		path = strings.Replace(path, "images/", "http://localhost:3000/", 1)
		size := info.Size()
		f := models.File{
			Path: path,
			Size: size,
		}
		files = append(files, f)
		return nil
	})
	if err != nil {
		return
	}
	files = files[1:]
	return
}

func DirwalkStrings(dir string) (files []string) {
	var err error
	err = filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		path = strings.Replace(path, "images/", "http://localhost:3000/", 1)
		path = After(path,"/")
		files = append(files, path)
		return nil
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	files = files[1:]
	return
}

func Mkdir(dir string)bool{
	realPath = GetRealPath()
	isexits,exitsErr := PathExists(realPath + dir)
	if exitsErr != nil {
		fmt.Println("Directory Exists")
	}
	if isexits == false {
		err := os.MkdirAll(realPath + dir, os.ModePerm)
		if err != nil {
			return false
		}
		fmt.Println("Directory: " + realPath + dir + ".")
		return true
	}
	return true
}

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func ExternalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // no es una direccion ipv4
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("are you connected to the network?")
}