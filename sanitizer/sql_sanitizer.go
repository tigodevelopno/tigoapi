package sanitizer

import (
	"regexp"
	"strings"
)

var ident = regexp.MustCompile(`(?i)^[a-z_][a-z0-9_$]*$`)
var params = regexp.MustCompile(`%([sIL])`)

func Escape(sql string) string {
	dest := make([]byte, 0, 2*len(sql))
	var escape byte
	for i := 0; i < len(sql); i++ {
		c := sql[i]
		escape = 0
		switch c {
		case 0:
			escape = '0'
			break
		case '\n':
			escape = 'n'
			break
		case '\r':
			escape = 'r'
			break
		case '\\':
			escape = '\\'
			break
		case '\'':
			escape = '\''
			break
		case '"':
			escape = '"'
			break
		case '\032':
			escape = 'Z'
		}
		if escape != 0 {
			dest = append(dest, '\\', escape)
		} else {
			dest = append(dest, c)
		}
	}
	return string(dest)
}

func StructuredSanitation(query string, args ...string) string {
	matches := params.FindAllStringSubmatch(query, -1)
	length := len(matches)
	argc := len(args)
	if argc > length {
		panic("too many arguments for escaped query")
	}
	if argc < length {
		panic("too few arguments for escaped query")
	}
	for i, match := range matches {
		arg := args[i]
		switch match[1] {
		case "L":
			query = strings.Replace(query, "%L", Literal(arg), 1)
		case "I":
			query = strings.Replace(query, "%I", Ident(arg), 1)
		case "s":
			query = strings.Replace(query, "%s", arg, 1)
		}
	}
	return query
}

func Literal(s string) string {
	p := ""
	if strings.Contains(s, `\`) {
		p = "E"
	}
	s = strings.Replace(s, `'`, `''`, -1)
	s = strings.Replace(s, `\`, `\\`, -1)
	return p + `'` + s + `'`
}

func Ident(s string) string {
	if IdentNeedsQuotes(s) {
		return QuoteIdent(s)
	} else {
		return s
	}
}

func IdentNeedsQuotes(s string) bool {
	if Reserved[strings.ToUpper(s)] {
		return true
	}
	return !ident.MatchString(s)
}

func QuoteIdent(s string) string {
	s = strings.Replace(s, `"`, `""`, -1)
	return `"` + s + `"`
}